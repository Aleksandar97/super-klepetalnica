var uporabnikiVzdevek = []  ;
var AliasVzdevek = [] ; 

/**
 * Zamenjava kode smeška s sliko (iz oddaljenega strežnika
 * https://sandbox.lavbic.net/teaching/OIS/gradivo/{smesko}.png)
 * 
 * @param vhodnoBesedilo sporočilo, ki lahko vsebuje kodo smeška
 */
function dodajSmeske(vhodnoBesedilo) {
  var preslikovalnaTabela = {
    ";)": "wink.png",
    ":)": "smiley.png",
    "(y)": "like.png",
    ":*": "kiss.png",
    ":(": "sad.png"
  }
  for (var smesko in preslikovalnaTabela) {
    vhodnoBesedilo = vhodnoBesedilo.split(smesko).join(
      "<img src='https://sandbox.lavbic.net/teaching/OIS/gradivo/" +
      preslikovalnaTabela[smesko] + "' />");
  }
  return vhodnoBesedilo;
}


/**
 * Čiščenje besedila sporočila z namenom onemogočanja XSS napadov
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementEnostavniTekst(sporocilo) {
  var jeSmesko = sporocilo.indexOf("https://sandbox.lavbic.net/teaching/OIS/gradivo/") > -1;
  jeSmesko = sporocilo.indexOf("&#9756;") > -1;
  if (jeSmesko) {
    sporocilo = 
      sporocilo.split("<").join("&lt;")
               .split(">").join("&gt;")
               .split("&lt;img").join("<img")
               .split("png' /&gt;").join("png' />");
    return divElementHtmlTekst(sporocilo);
  } else {
    return $('<div style="font-weight: bold"></div>').text(sporocilo);  
  }
}


/**
 * Prikaz "varnih" sporočil, t.j. tistih, ki jih je generiral sistem
 * 
 * @param sporocilo začetno besedilo sporočila
 */
function divElementHtmlTekst(sporocilo) {
  return $('<div></div>').html('<i>' + sporocilo + '</i>');
}


/**
 * Obdelaj besedilo, ki ga uporabnik vnese v obrazec na spletni strani, kjer
 * je potrebno ugotoviti ali gre za ukaz ali za sporočilo na kanal
 * 
 * @param klepetApp objekt Klepet, ki nam olajša obvladovanje 
 *        funkcionalnosti uporabnika
 * @param socket socket WebSocket trenutno prijavljenega uporabnika
 */
function procesirajVnosUporabnika(klepetApp, socket) {
  var sporocilo = $('#poslji-sporocilo').val();
  sporocilo = dodajSmeske(sporocilo);
  var sistemskoSporocilo;
  // Če uporabnik začne sporočilo z znakom '/', ga obravnavaj kot ukaz
  if (sporocilo.charAt(0) == '/') {
    sistemskoSporocilo = klepetApp.procesirajUkaz(sporocilo);
    if (sistemskoSporocilo) {
      var zasebnoMsg = sistemskoSporocilo.split(" ");
       if(zasebnoMsg[0]=='(zasebno'){
          zasebnoMsg[2] = zasebnoMsg[2].substring(0,zasebnoMsg[2].length-2);
          zasebnoMsg.splice(0, 2);
          console.log(zasebnoMsg[0]);
          var obstaja = false;
          var indexVzdevek;
          for(var i =0 ;i<uporabnikiVzdevek.length;i++){
            if(zasebnoMsg[0]==uporabnikiVzdevek[i] && AliasVzdevek[i]!=null){
              obstaja=true;
              indexVzdevek=i;
            }
          }
          if(obstaja){
          var bes = '' ;
          for(var j =1 ;j<zasebnoMsg.length;j++){
            bes += zasebnoMsg[j]+' ';
        
          }
         sistemskoSporocilo ='(zasebno za ' + AliasVzdevek[indexVzdevek]+' ('+uporabnikiVzdevek[indexVzdevek]+')):'+bes;
      }
    }
      $('#sporocila').append(divElementHtmlTekst(sistemskoSporocilo));
  }
  // Če gre za sporočilo na kanal, ga posreduj vsem članom kanala
  } else {
    sporocilo = filtirirajVulgarneBesede(sporocilo);
    klepetApp.posljiSporocilo(trenutniKanal, sporocilo);
    $('#sporocila').append(divElementEnostavniTekst(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
    $('#sporocila').append(DodajSlike(sporocilo));
    $('#sporocila').scrollTop($('#sporocila').prop('scrollHeight'));
  }

  $('#poslji-sporocilo').val('');
}

// Branje vulgarnih besed v seznam
var vulgarneBesede = [];
$.get('/swearWords.txt', function(podatki) {
  vulgarneBesede = podatki.split('\r\n'); 
});

/**
* Iz podanega niza vse besede iz seznama vulgarnih besed zamenjaj z enako dolžino zvezdic (*).
* 
* @param vhodni niz
*/
function filtirirajVulgarneBesede(vhod) {

 for (var i in vulgarneBesede) {
   var re = new RegExp('\\b' + vulgarneBesede[i] + '\\b', 'gi');
   vhod = vhod.replace(re, "*".repeat(vulgarneBesede[i].length));
 }
 
 return vhod;
}


var socket = io.connect();
var trenutniVzdevek = "";
var trenutniKanal = "";

// Poačakj, da se celotna stran naloži, šele nato začni z izvajanjem kode
$(document).ready(function() {
  var klepetApp = new Klepet(socket);
  
  // Prikaži rezultat zahteve po spremembi vzdevka
  socket.on('vzdevekSpremembaOdgovor', function(rezultat) {
    var sporocilo;
    if (rezultat.uspesno) {
      trenutniVzdevek = rezultat.vzdevek;
      $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
      sporocilo = 'Prijavljen si kot ' + rezultat.vzdevek + '.';
    } else {
      sporocilo = rezultat.sporocilo;
    }
    $('#sporocila').append(divElementHtmlTekst(sporocilo));
  });
  
  // Prikaži rezultat zahteve po spremembi kanala
  socket.on('pridruzitevOdgovor', function(rezultat) {
    trenutniKanal = rezultat.kanal;
    $('#kanal').text(trenutniVzdevek + " @ " + trenutniKanal);
    $('#sporocila').append(divElementHtmlTekst('Sprememba kanala.'));
  });
  
  // Prikaži prejeto sporočilo
  socket.on('sporocilo', function (sporocilo) {
    //var novElement = divElementEnostavniTekst(sporocilo.besedilo);
    var user = sporocilo.besedilo;
    user= user.split(" ");
    var zasebno = false;
    if(user[0].substring(user[0].length - 1,user[0].length)==':'){
      user[0]=user[0].substring(0, user[0].length - 1);
      zasebno=true;
    }
    var obstaja = false;
    var indexVzdevek;
    for(var i =0 ;i<uporabnikiVzdevek.length;i++){
      if(user[0]==uporabnikiVzdevek[i] && AliasVzdevek[i]!=null){
        obstaja=true;
        indexVzdevek=i;
      }
    }
    if(obstaja){
      var bes = '' ;
      for(var j =1 ;j<user.length;j++){
        bes += user[j]+' ';
        
      }
      var message;
      if(zasebno){
        message = AliasVzdevek[indexVzdevek]+' ('+uporabnikiVzdevek[indexVzdevek]+'):'+bes;
      }else{
        message = AliasVzdevek[indexVzdevek]+' ('+uporabnikiVzdevek[indexVzdevek]+') '+bes;
      }
      message = divElementEnostavniTekst(message);
      $('#sporocila').append(message);
      
    } else {
      var novElement = divElementEnostavniTekst(sporocilo.besedilo);
      $('#sporocila').append(novElement);
    }
    var slike = DodajSlike(sporocilo.besedilo);
    $('#sporocila').append(slike)
    
  });
  
  // Prikaži seznam kanalov, ki so na voljo
  socket.on('kanali', function(kanali) {
    $('#seznam-kanalov').empty();

    for(var i in kanali) {
      if (kanali[i] != '') {
        $('#seznam-kanalov').append(divElementEnostavniTekst(kanali[i]));
      }
    }
  
    // Klik na ime kanala v seznamu kanalov zahteva pridružitev izbranemu kanalu
    $('#seznam-kanalov div').click(function() {
      klepetApp.procesirajUkaz('/pridruzitev ' + $(this).text());
      $('#poslji-sporocilo').focus();
    });
  });
  
  // Prikaži seznam trenutnih uporabnikov na kanalu
  socket.on('uporabniki', function(uporabniki) {
    $('#seznam-uporabnikov').empty();
    for (var i=0; i < uporabniki.length; i++) {
      uporabnikiVzdevek[i] = uporabniki[i];
    }
    for (var i=0; i < uporabniki.length; i++) {
      if(AliasVzdevek[i] != null){
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(AliasVzdevek[i] + ' (' + uporabnikiVzdevek[i] + ')'));
      } else {
        $('#seznam-uporabnikov').append(divElementEnostavniTekst(uporabnikiVzdevek[i]));
      }
    }
    $('#seznam-uporabnikov div').click(function() {
      var name = $(this).text();
      name = name.split(" ");
      console.log(name[1]);
      if(name[1] != null){
        if(name[1].substring(0,1)=='(' ){
          name[0] = name[1].substring(1,name[1].length-1);
        }
      }
      console.log(name[0]);
      klepetApp.procesirajUkaz('/zasebno "' + name[0] + '" "&#9756;"' );
      $('#poslji-sporocilo').focus();
      var Element = divElementEnostavniTekst('(zasebno za ' + $(this).text() + '): &#9756;' );
      $('#sporocila').append(Element);
    });
  });
  
  // Seznam kanalov in uporabnikov posodabljaj vsako sekundo
  setInterval(function() {
    socket.emit('kanali');
    socket.emit('uporabniki', {kanal: trenutniKanal});
  }, 1000);

  $('#poslji-sporocilo').focus();
  
  // S klikom na gumb pošljemo sporočilo strežniku
  $('#poslji-obrazec').submit(function() {
    procesirajVnosUporabnika(klepetApp, socket);
    return false;
  });
});

function DodajSlike(sporocilo) {
  var words = sporocilo.split(' ');
  var temp = '';
  for (var i in words) {
    if (new RegExp('\\b(https|http):\\/\\/+\\S+\\.(png|gif|jpg)\\b').test(words[i])) { 
      temp += '<img src="' + words[i] + '" width="200px" height="auto"></img><br />';
    }
  }
  return $('<div style="margin-left: 20px;"></div>').html(temp);
}

//Zasebno sporocilo uporabi samo vzdevek brez nadimek in ()
//Alias
socket.on('preimenuj', function(preimenuj) {
   for(var i =0 ; i< uporabnikiVzdevek.length;i++){
     if(preimenuj.vzdevek == uporabnikiVzdevek[i]){
        AliasVzdevek[i] = preimenuj.alias;
     }
   }
});

